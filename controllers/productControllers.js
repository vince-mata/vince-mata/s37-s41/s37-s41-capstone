const Product = require("../models/Product");
const User = require("../models/User");


// Creating/selling a product.
/*
	Logic:
	1. Check if the user logged in is an admin.
	2. If it is an admin, add the product to the database.
	3. If not, deny request.
*/
module.exports.sellProduct = (sessionData, productData) => {
	return Product.find({}).then(result => {
		if(sessionData.isAdmin){
			let newProduct = new Product({
				name: productData.name,
				description: productData.description,
				price: productData.price
			}); // end let newProduct
			return newProduct.save().then((product, err) => {
				if(err){
					return false;
				}
				else{
					return true;
				} // end else if(err)
			}); // end return newProduct.save().then()
		} // end if(sessionData.isAdmin)
		else{
			return false;
		}
	}); // return Product.find().then()
}; // end module.exports.sellProduct

// Retrieve all active products
/*
	Logic:
	1. Find all items that is in stock/active.
	2. Return all gathered data.
*/
module.exports.viewAllActiveProducts = () => {
	return Product.find({isActive: true}).then(result => result);
}; // end module.exports.getAllActiveProducts

module.exports.viewAllProducts = (sessionData) => {
	return Product.find({}).then((result, err)=> {
		if(err){
			return false;
		}
		else{
			if(sessionData.isAdmin){
				return result;
			}
			else{
				return false;
			}
		}
	}); // return Product.find().then()
}; // end module.exports.getAllActiveProducts

// Retrieve a single product.
/*
	Logic:
	1. Find the product data that matches the parameter ID.
	2. Return the data.
*/
module.exports.viewProduct = (productId) => {
	return Product.findById(productId).then((product, err) => {
		if(err){
			return false;
		}
		else{
			return product;
		} // end else if(err)
	}); // end return Product.findById().then()
}; // end module.exports.viewProduct 

// Updating a product
/*
	Logic:
	1. Find the product data that matches the parameter ID.
	2. Check if the session user is an admin.
		- If it is an admin, then update the product data from req.body.
		- If not, deny request
*/
module.exports.updateProduct = (reqParams, reqBody, sessionData) => {
	let updatedProduct = {
		name: reqBody.name,
		description: reqBody.description,
		price: reqBody.price
	};
	return Product.findById(reqParams.productId).then((result, err) => {
		if(err){
			return false;
		} // end if(err)
		else{
			if(sessionData.isAdmin){
				result.name = reqBody.name;
				result.description = reqBody.description;
				result.price = reqBody.price;
				return result.save().then((updatedProduct, err) => {
					if(err){
						return false;
					} // end if(err)
					else{
						return true;
					} // end else if(err)
				}); // end return result.save().then()
			} // end if(sessionData.isAdmin)
			else{
				return false;
			} // end else if(sessionData.isAdmin)
		} // end else if(err)
	}); // end return Product.findById().then()
}; // end module.exports.updateProduct

// Archiving a product
/*
	Logic:
	1. Find the product data that matches the parameter ID.
	2. Check if the session user is an admin.
		- If it is an admin, then archive the product.
		- TODO: Remove the item in every cart that includes the item.
		- If not, deny request
*/
module.exports.archiveProduct = (reqParams, sessionData) => {
	return Product.findById(reqParams.productId).then((result, err) => {
		if(err){
			return false;
		} // end if(err)
		else{
			if(sessionData.isAdmin){
				result.isActive = false;
				return result.save().then((archivedProduct, err) => {
					if(err){
						return false;
					}
					else{
						return true;
					}
				}); // end return result.save().then()
			} // end if(sessionData.isAdmin)
			else{
				return false;
			} // else if(sessionData.isAdmin)
		} // end else if(err)
	}); // end return Product.findById
}; // end module.product.archiveProduct


// Archiving a product
/*
	Logic:
	1. Find the product data that matches the parameter ID.
	2. Check if the session user is an admin.
		- If it is an admin, then archive the product.
		- If not, deny request
*/
module.exports.unarchiveProduct = (reqParams, sessionData) => {
	return Product.findById(reqParams.productId).then((result, err) => {
		if(err){
			return false;
		} // end if(err)
		else{
			if(sessionData.isAdmin){
				result.isActive = true;
				return result.save().then((archivedProduct, err) => {
					if(err){
						return false;
					}
					else{
						return true;
					}
				}); // end return result.save().then()
			} // end if(sessionData.isAdmin)
			else{
				return false;
			} // else if(sessionData.isAdmin)
		} // end else if(err)
	}); // end return Product.findById
}; // end module.product.unarchiveProduct





