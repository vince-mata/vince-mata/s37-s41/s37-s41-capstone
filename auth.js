//Json Web Token

const jwt = require("jsonwebtoken");

const secret = "EcommerceAPI";

//Create Token

module.exports.createAccessToken = (user) => {

//User Login Info

const data = {
		id: user._id,
		email: user.email,
		isAdmin: user.isAdmin
	}	

//Generate JWT and Secret

return jwt.sign(data, secret, {});

}

//Verify Token

module.exports.verify = (req, res, next) => {

	let token = req.headers.authorization;

	if(typeof token !== "undefined") {

		token = token.slice(7, token.length);

		return jwt.verify(token, secret, (err, data) => {

			if(err){
				return res.send({auth:"failed"});

			} else {
				next();
			}
		})

	} else {
		return res.send({auth: "failed"});
	}
};

//Token Decryption

module.exports.decode = (token) => {

	if(typeof token !== "undefined"){

		token = token.slice(7, token.length);

		return jwt.verify(token, secret, (err, data) => {

			if(err) {
				return null

			} else {
				return jwt.decode(token, {complete: true}).payload;
			}
		})

	} else {
		return null
	}
};