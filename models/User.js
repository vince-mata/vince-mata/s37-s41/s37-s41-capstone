const mongoose = require("mongoose");

const userSchema = new mongoose.Schema({
	email: {
		type: String,
		required: [true, "Email is required."]
	},
	password: {
		type: String,
		required: [true, "Password is required."]
	},
	isAdmin: {
		type: Boolean,
		default: false
	},
	cart: {
		products: [
			{
				productId: {
					type: String,
					required: [true, "Product ID is required"]
				},
				name: {
					type: String,
					required: [true, "Product name is required"]
				},
				quantity: {
					type: Number,
					required: [true, "Quantity is required."]
				},
				price: {
					type: Number,
					default: 0,
					required: [true, "Price is required"]
				}
			}
		],
		totalAmount: {
			type: Number,
			default: 0,
			required: [true, "Total amount is required"],
		}
	},
	orders: [
		{
			orderId: {
				type: String,
				required: [true, "Order ID is required"]
			},
			totalAmount: {
				type: Number,
				required: [true, "Total amount is required"]
			},
			products: [
				{
					productId: {
						type: String,
						required: [true, "Product ID is required."]
					},
					name: {
						type: String,
						required: [true, "Product name is required."]
					},
					quantity: {
						type: Number,
						required: [true, "Quantity is required."]
					},
					price: {
						type: Number,
						default: 0,
						required: [true, "Price is required."]
					}
				}
			]
		}
	]
}); // end const userSchema

module.exports = mongoose.model("User", userSchema);


