const mongoose = require("mongoose");
const orderSchema = new mongoose.Schema({
	products: [
		{
			productId: {
				type: String,
				required: [true, "Product ID is required."]
			},
			name: {
				type: String,
				required: [true, "Product name is required."]
			},
			quantity: {
				type: Number,
				required: [true, "Quantity is required."]
			},
			price: {
				type: Number,
				default: 0,
				required: [true, "Price is required."]
			}
		}
	],
	orderedBy: {
		userId: {
			type: String,
			required: [true, "User ID is required."]
		},
		email: {
			type: String,
			required: [true, "Email is required."]
		}
	},
	totalAmount: {
		type: Number,
		default: 0,
		required: [true, "Total amount is required."]
	},
	purchasedOn: {
		type: Date,
		default: new Date()
	}
}); // end const orderSchema

module.exports = mongoose.model("Order", orderSchema);


