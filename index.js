//Modules

const express = require("express");
const mongoose = require("mongoose");
const cors = require("cors");

//Routes Modules
const userRoutes = require("./routes/userRoutes");
const productRoutes = require("./routes/productRoutes");
const orderRoutes = require("./routes/orderRoutes");

//Server
const port = 4000;
const app = express();
app.use(cors());
app.use(express.json());
app.use(express.urlencoded({extended: true}));

//Routes
app.use("/users", userRoutes);
app.use("/products", productRoutes);
app.use("/orders", orderRoutes);

//MongoDB

mongoose.connect("mongodb+srv://admin:admin123@course-booking.l6tpp.mongodb.net/s37-s41?retryWrites=true&w=majority", {
		useNewUrlParser: true,
		useUnifiedTopology: true
});

let db = mongoose.connection;
db.on('error', () => console.error.bind(console,'Connection Error'));
db.once('open', () => console.log('Now connected to MongoDB Atlas'));

//Port
app.listen(process.env.PORT || port, () => {
	console.log(`Server is running on port ${process.env.PORT || port}`)
});





