const express = require("express");
const router = express.Router();
const auth = require("../auth");
const orderController = require("../controllers/orderControllers");

// Route for viewing order from the authenticated user.
router.get("/myorders", auth.verify, (req, res) => {
	const userData = auth.decode(req.headers.authorization);
	orderController.viewOrdersFromUser(userData).then(result => res.send(result));
});

// Route for retrieving all orders
router.get("/", auth.verify, (req, res) => {
	const userData = auth.decode(req.headers.authorization)
	orderController.viewAllOrders(userData).then(result => res.send(result));
});

// Route for ordering an item
router.post("/buynow", auth.verify, (req, res) => {
	const userData = auth.decode(req.headers.authorization);
	orderController.orderProduct(userData, req.body).then(result => res.send(result));
});

// Route for ordering items in the cart
router.get("/checkoutcart", auth.verify, (req, res) => {
	const userData = auth.decode(req.headers.authorization);
	orderController.checkoutFromCart(userData).then(result => res.send(result));
});



module.exports = router;
